import { LeaguePoints } from "./LeaguePoints";


export interface GameResult {
    readonly leaguePoints: LeaguePoints[]; 
}

class WinnerGameResult implements GameResult {

    private winningTeam: string;
    private losingTeam: string;
    
    constructor(winningTeam: string, losingTeam: string) {
        this.winningTeam = winningTeam
        this.losingTeam = losingTeam
    }

    get leaguePoints(): LeaguePoints[] {
        return [ 
            new LeaguePoints(this.winningTeam, 3),
            new LeaguePoints(this.losingTeam, 0)
        ]
    }
}

class DrawGameResult implements GameResult {

    private teamA: string;
    private teamB: string;
    
    constructor(teamA: string, teamB: string) {
        this.teamA = teamA
        this.teamB = teamB
    }

    get leaguePoints(): LeaguePoints[] {
        return [
            new LeaguePoints(this.teamA, 1),
            new LeaguePoints(this.teamB, 1)
        ]
    }
}

export class GameResultFactory {
    
    static createResultFromLine(line: string): GameResult {

        const {groups: {teamA, scoreA, teamB, scoreB}} = /(?<teamA>.*?)\s(?<scoreA>\d),\s(?<teamB>.*?)\s(?<scoreB>\d)/u
            .exec(line)

        if(scoreA > scoreB) {
            return new WinnerGameResult(teamA, teamB)
        } 

        if(scoreB > scoreA) {
            return new WinnerGameResult(teamB, teamA)
        }
            
        return new DrawGameResult(teamA, teamB);
    }
}