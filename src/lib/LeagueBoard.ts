import { LeaguePoints } from "./LeaguePoints";


export class Ranking {
    private _rank: number;
    private _entry: LeaguePoints;

    constructor(rank: number, entry: LeaguePoints) {
        this._rank = rank
        this._entry = entry;
    }

    get rank(): number { return this._rank }
    get entry(): LeaguePoints { return this._entry }

    toString(): string {
        return `${this.rank}. ${this.entry.team}, ${this.entry.points} pt${ this.entry.points === 1 ? "" : "s"}`
    }
}

export class LeagueBoard {

    private teamPoints: {[s: string]: LeaguePoints} = {}
    
    addPoints(points: LeaguePoints): void {    
        if(!this.teamPoints[points.team]) {
            this.teamPoints[points.team] = new LeaguePoints(points.team, 0)
        }

        this.teamPoints[points.team].addPoints(points);
    }

    get scoreBoard(): LeaguePoints[]  {
        return Object.values(this.teamPoints).sort(LeaguePoints.sort);
    }

    get rankings(): Ranking[] {
        const rankings: Ranking[] = []
        this.scoreBoard.forEach(
            (pointsEntry, index) => {
                const lastRanking = rankings.length 
                    ? rankings[rankings.length -1] 
                    : null 
                rankings.push(LeagueBoard.createRanking(pointsEntry, index + 1, lastRanking));
            }
        )
        return rankings;
    }

    static createRanking(entry: LeaguePoints, proposedRank: number, lastRanking: Ranking): Ranking {
        if(lastRanking !== null && lastRanking.entry.points === entry.points) {
            return new Ranking(lastRanking.rank, entry);
        }

        return new Ranking(proposedRank, entry);
    }
}