import { Writable } from "stream";
import { LeagueBoard } from "./LeagueBoard";
import { loadFile } from "./Common";
import { GameResultFactory } from "./GameResult";


export class LeaguePrinter {

    generateRankingPrint(filename:string): string {
        let league = new LeagueBoard()

        const input = loadFile(filename);
        input.split("\n")
            .forEach(
                line => {
                    GameResultFactory.createResultFromLine(line)
                        .leaguePoints
                        .forEach(lp => league.addPoints(lp))
                }
            ) 

        return league.rankings
            .map(r => r.toString() )    
            .join("\n")
    }
}