import * as fs from "fs"

export function loadFile(path: string): string {
    return fs.readFileSync(path, "utf8")
}