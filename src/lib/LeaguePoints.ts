

  

export class LeaguePoints {
    team: string;
    points: number;

    constructor(team: string, points: number) {
        this.team = team;
        this.points = points
    }

    addPoints(other: LeaguePoints) {
        this.points = this.points + other.points
    }

    static sort(a: LeaguePoints, b: LeaguePoints): number {
        if(a.points < b.points) {
            return 1;
        }

        if(a.points > b.points) {
            return -1;
        }

        return LeaguePoints.nameSort(a, b);
    }

    static nameSort(a: LeaguePoints, b: LeaguePoints) {
        if ( a.team < b.team ) {
            return -1;
        }

        if ( a.team > b.team ) {
            return 1;
        }

        return 0;
    }
}