
import * as commandLineArgs from "command-line-args"
import * as path from "path"
import { LeaguePrinter } from "./lib/LeaguePrinter";

// setup options 
const optionDefinitions = [
    { 
        name: 'file', 
        alias: 'f', 
        type: String, 
        multiple: false, 
        defaultOption: true,
        defaultValue: path.resolve("resources", "test_input.txt"),
        description: "The file path for all match results. Defaults to `resources/test_input.txt`"
    }
  ]

// load app input
const options = commandLineArgs(optionDefinitions)
const {file} = options

// run printer
let output = new LeaguePrinter().generateRankingPrint(file)
console.log(output)