**About**

This is a Soccer League ranking app. It takes an input file with match results
and print the league rankings based on the results.

This application requires *node* to be built and run.

## Install dependencies 

You will need to install all the dependencies.

> npm install

---

## Run Tests

The unit test live in the *tests* folder.

> npm run test

---

## Build the soccer_league.js

Build the application which is written to *build/soccer_league.js*

> npm run build 


# Run the Demo

The demo can be run by executing

> npm run demo

or 

> node build/soccer_league.js  



# Specify match result file

soccer_league.js takes a single optional *--file* argument. By default it will
load *resources/test_input.txt*.

> node build/soccer_league.js --file resources/test_input.txt
