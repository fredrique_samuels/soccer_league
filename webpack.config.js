const path = require('path');

// calculated paths
const DIST_DIR = path.resolve(__dirname, 'build');
const SRC_ROOT_DIR = path.resolve(__dirname, 'src');

module.exports = {
    entry: {
        soccer_league: './src/soccer_league.ts'
    },
    target: 'node',
    output: {
        path: DIST_DIR,
        filename: '[name].js',
    },
    devtool: 'source-map',
    resolve: {
        extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
        modules: [
            SRC_ROOT_DIR,
            path.join(__dirname, './node_modules/')
        ]
    },
    module: {
        rules: [
            {
                // this is so that we can compile any React,
                // ES6 and above into normal ES5 syntax
                test: /\.(js|jsx)$/,
                // we do not want anything from node_modules to be compiled
                exclude: /node_modules/,
                use: [{
                    loader: 'babel-loader',
                    options: {},
                }]


            },
            {
                test: /\.(ts|tsx)$/,
                loader: 'ts-loader'
            },
            { enforce: "pre", test: /\.(js|jsx)$/, loader: "source-map-loader" },
            {
                test: /\.txt$/,
                use: 'raw-loader'
            }
        ]
    }
};