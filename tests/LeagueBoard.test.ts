import {deepStrictEqual, fail} from 'assert'
import { GameResult, GameResultFactory } from "../src/lib/GameResult";
import {LeaguePoints} from "../src/lib/LeaguePoints"
import {LeagueBoard} from "../src/lib/LeagueBoard"

describe('LeagueBoard', function() {

    it('points-added', function () {
        // given
        let board = new LeagueBoard();

        // when
        board.addPoints(new LeaguePoints("TeamA", 3));
        board.addPoints(new LeaguePoints("TeamA", 1));

        //then
        const [entry] = board.scoreBoard;
        deepStrictEqual(entry.team, "TeamA")
        deepStrictEqual(entry.points, 4)
        
    });

    it('sort-order', function () {
        // given
        let board = new LeagueBoard();

        // when
        board.addPoints(new LeaguePoints("TeamA", 3));
        board.addPoints(new LeaguePoints("TeamB", 2));
        board.addPoints(new LeaguePoints("TeamC", 0));
        board.addPoints(new LeaguePoints("TeamD", 2));

        //then
        const [entry0, entry1, entry2, entry3] = board.scoreBoard;
        deepStrictEqual(entry0.team, "TeamA")
        deepStrictEqual(entry1.team, "TeamB")
        deepStrictEqual(entry2.team, "TeamD")
        deepStrictEqual(entry3.team, "TeamC")
    });


    it('ranking', function () {
        // given
        let board = new LeagueBoard();

        // when
        board.addPoints(new LeaguePoints("TeamA", 3));
        board.addPoints(new LeaguePoints("TeamB", 2));
        board.addPoints(new LeaguePoints("TeamC", 0));
        board.addPoints(new LeaguePoints("TeamD", 2));

        //then
        const [entry0, entry1, entry2, entry3] = board.rankings;

        deepStrictEqual(entry0.entry.team, "TeamA")
        deepStrictEqual(entry0.rank, 1)

        deepStrictEqual(entry1.entry.team, "TeamB")
        deepStrictEqual(entry1.rank, 2)

        deepStrictEqual(entry2.entry.team, "TeamD")
        deepStrictEqual(entry2.rank, 2)

        deepStrictEqual(entry3.entry.team, "TeamC")
        deepStrictEqual(entry3.rank, 4)
    });

    it('ranking-to-string', function () {
        // given
        let board = new LeagueBoard();

        // when
        board.addPoints(new LeaguePoints("TeamA", 0));

        // then
        deepStrictEqual(board.rankings[0].toString(), "1. TeamA, 0 pts")

        // when
        board.addPoints(new LeaguePoints("TeamA", 1));

        // then
        deepStrictEqual(board.rankings[0].toString(), "1. TeamA, 1 pt")

        // when
        board.addPoints(new LeaguePoints("TeamA", 1));

        // then
        deepStrictEqual(board.rankings[0].toString(), "1. TeamA, 2 pts")
    });

});
