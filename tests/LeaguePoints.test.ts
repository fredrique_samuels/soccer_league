import {deepStrictEqual, fail} from 'assert'
import { GameResult, GameResultFactory } from "../src/lib/GameResult";
import {LeaguePoints} from "../src/lib/LeaguePoints"
import {LeagueBoard} from "../src/lib/LeagueBoard"

describe('LeaguePoints', function() {

    it('sort-by-points', function () {
        // given
        const points = [
            new LeaguePoints("teamA", 0),
            new LeaguePoints("teamB", 2),
            new LeaguePoints("teamC", 1)
        ]

        // when
        const [a, b, c] = points.sort(LeaguePoints.sort)

        // then
        deepStrictEqual(a.team, "teamB");
        deepStrictEqual(b.team, "teamC");
        deepStrictEqual(c.team, "teamA");
    });

    it('order-by-name', function () {
        // given
        const points = [
            new LeaguePoints("teamB", 3),
            new LeaguePoints("teamC", 2),
            new LeaguePoints("teamA", 2)
        ]

        // when
        const [a, b, c] = points.sort(LeaguePoints.sort)

        // then
        deepStrictEqual(a.team, "teamB");
        deepStrictEqual(b.team, "teamA");
        deepStrictEqual(c.team, "teamC");
    });
});
