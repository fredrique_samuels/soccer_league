import {deepStrictEqual, fail} from 'assert'
import { LeaguePrinter } from '../src/lib/LeaguePrinter';
import { loadFile } from '../src/lib/Common';

describe('LeaguePrinter', function() {

    it('print', function () {
        // given
        const printer = new LeaguePrinter();
        const expected = loadFile("./resources/test_output.txt")

        // when
        const actual = printer.generateRankingPrint("./resources/test_input.txt") 

        // then
        deepStrictEqual(actual, expected);
    });
});
