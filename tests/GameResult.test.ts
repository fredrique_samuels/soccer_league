import {deepStrictEqual} from 'assert'
import { GameResult, GameResultFactory } from "../src/lib/GameResult";
import {LeaguePoints} from "../src/lib/LeaguePoints"

describe('GameResult', function() {

    it('winning-game-points', function () {
        //given
        let result1: GameResult = GameResultFactory.createResultFromLine("Tarantulas 1, FC Awesome 0")

        // then
        assertWinningLeaguePoints(result1.leaguePoints, "Tarantulas", "FC Awesome")

        //given
        let result2: GameResult = GameResultFactory.createResultFromLine("Tarantulas 1, FC Awesome 3")

        // then
        assertWinningLeaguePoints(result2.leaguePoints, "FC Awesome", "Tarantulas")
    });


    it('draw-game-points', function () {
        //given
        let result: GameResult = GameResultFactory.createResultFromLine("Tarantulas 1, FC Awesome 1")

        // then
        const [teamA, teamB] = result.leaguePoints

        // then
        deepStrictEqual(teamA.team, "Tarantulas")
        deepStrictEqual(teamA.points, 1)

        deepStrictEqual(teamB.team, "FC Awesome")
        deepStrictEqual(teamB.points, 1)
    });

    function assertWinningLeaguePoints(leaguePoints: LeaguePoints[], 
                        expectedWinner: string, 
                        expectedLoser: string) {
        // when
        const [winner, loser] = leaguePoints

        // then
        deepStrictEqual(winner.team, expectedWinner)
        deepStrictEqual(winner.points, 3)

        deepStrictEqual(loser.team, expectedLoser)
        deepStrictEqual(loser.points, 0)
    }

});
